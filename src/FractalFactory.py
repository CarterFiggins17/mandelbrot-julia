import sys
import mandelbrot, julia, MandelbrotSqur5, Gradient

def findFractal(picType, gradient, frac):
    if picType == "Mandelbrot":  # give a gradient
        picture = mandelbrot.mandelbrot(gradient)
    elif picType == "Julia":
        picture = julia.julia(gradient, frac.config)
    elif picType == "MandelbrotSqur5":
        picture = MandelbrotSqur5.MandelbrotSqur5(gradient)
    else:
        print("ERROR in type")
        sys.exit(1)
    return picture


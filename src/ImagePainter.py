import sys
from tkinter import Tk, Canvas, PhotoImage, mainloop


class ImagePainter():

    def __init__(self, config, name):   #has all the fracs info from config
        self.config = config
        self.name = name
        self.size = config["pixels"]
        self.img = None
        self.window = Tk()
        self.x = 0
        self.y = 0

    def paint(self, gradient, type):
        """Paint a Fractal image into the TKinter PhotoImage canvas.  Assumes the
        image is 640x640 pixels.
        This function displays a really handy status bar so you can see how far
        along in the process the program is."""

        # Figure out how the boundaries of the PhotoImage relate to coordinates on
        # the imaginary plane.
        minx = float(self.config['centerx']) - (float(self.config['axislength']) / 2.0)
        maxx = self.config['centerx'] + (self.config['axislength'] / 2.0)
        miny = self.config['centery'] - (self.config['axislength'] / 2.0)

        # At this scale, how much length and height on the imaginary plane does one
        # pixel take?
        rows = 64 #loding bar size
        pixelsize = abs(maxx - minx) / self.size

        portion = int(self.size / rows)
        for col in range(self.size):
            if col % portion == 0:
                # Update the status bar each time we complete 1/64th of the rows
                pips = col // portion
                pct = col / self.size
                print(f"{self.name} ({self.size}x{self.size}) {'=' * pips}{'_' * (rows - pips)} {pct:.0%}", end='\r', file=sys.stderr)
            for row in range(self.size):
                self.x = minx + col * pixelsize
                self.y = miny + row * pixelsize
                self.color = type.count(complex(self.x, self.y))

                self.img.put(self.color, (col, row))
        print(f"{self.name} ({self.size}x{self.size}) ================================================================ 100%",
              file=sys.stderr)

        # Display the image on the screen
        canvas = Canvas(self.window, width=self.size, height=self.size, bg=gradient.colors[0])
        canvas.pack()
        canvas.create_image((self.size/2), (self.size/2), image=self.img, state="normal")




    def makePic(self, type, gradient):
        self.img = PhotoImage(width=self.size, height=self.size)
        self.paint(gradient, type)

        self.img.write(self.name + ".png")
        print(f"Wrote image {self.name}.png")
        mainloop()





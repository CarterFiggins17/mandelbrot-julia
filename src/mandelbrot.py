
from Fractal import Fractal

class mandelbrot(Fractal):

    def __init__(self, gradient):
        self.gradient = gradient


    def count(self, c):
        """Return the color of the current pixel within the Mandelbrot set"""
        z = complex(0, 0) # z0

        # TODO: Make it easier to support more colors in the gradient
        for i in range(len(self.gradient.colors)):
            z = z * z + c # Get z1, z2, ...
            if abs(z) > 2:
                return self.gradient.colors[i] # The sequence is unbounded
        # TODO: Support more than 100 iterations as the bailout condition
        return self.gradient.colors[len(self.gradient.colors) - 1] # Indicate a bounded sequence









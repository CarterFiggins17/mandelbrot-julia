from Gradient import Gradient
import Color


class BlackAndWhite(Gradient):

    def __init__(self, iterations):
        super().__init__(iterations)
        self.start = Color.Color(0, 0, 0)
        self.stop = Color.Color(255, 255, 255)



from Fractal import Fractal

class MandelbrotSqur5(Fractal):

    def __init__(self, gradient):
        self.gradient = gradient


    def count(self, c):
        """Return the color of the current pixel within the Mandelbrot set"""
        z = complex(0, 0) # z0

        for i in range(len(self.gradient.colors)):
            z = z * z * z * z * z * z + c  # Get z1, z2, ...
            if abs(z) > 2:
                return self.gradient.colors[i] # The sequence is unbounded
        return self.gradient.colors[len(self.gradient.colors) - 1] # Indicate a bounded sequence






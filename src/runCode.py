import sys
import Config, ImagePainter, FractalFactory, GradientFactory

def runCode():
    if len(sys.argv) < 2:
        print("Usage: mandelbrot.py FRACTALNAME")
        name = "elephants.frac"
    else:
        name = sys.argv[1]  # if len(sys.argv) < 2:

    frac = Config.Config(name)
    image = ImagePainter.ImagePainter(frac.config, name)

    picType = frac.config["type"]

    gradient = GradientFactory.makeGradient(frac.config["iterations"])

    gradient.makeGradient()

    picture = FractalFactory.findFractal(picType, gradient, frac)

    image.makePic(picture, gradient)
from Fractal import Fractal

class julia(Fractal):

    def __init__(self, gradient, config):
        self.gradient = gradient
        self.config = config

    def count(self, z):
        """Return the color of the current pixel within the Julia set"""
        # TODO: I understand that you can get more 'interesting' images when you
        #       use a different value for c.  Maybe I can encode this into the
        #       image configuration dictionary down below...
        x = int(self.config["creal"])
        y = int(self.config["cimag"])
        c = complex(x, y)  # c0

        # TODO: Make it easier to support more colors in the gradient
        for i in range(len(self.gradient.colors)):
            z = z * z + c  # Get z1, z2, ...
            if abs(z) > 2:
                return self.gradient.colors[i]  # The sequence is unbounded
        # TODO: Support more than 78 iterations as the bailout condition
        return self.gradient.colors[len(self.gradient.colors) - 1]  # Indicate a bounded sequence


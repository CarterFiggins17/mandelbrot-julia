

class Config():


    def __init__(self,name):
        self.name = name
        self.config = {}
        self.getFrac()

    def getFrac(self):
        filename = self.name
        filename = "../data/" + filename
        file = open(filename)

        for i in file:
            i = i.replace(" ", "")
            data = i.split(':')
            data[1] = data[1].replace("\n", "")
            data[0] = data[0].lower()
            if data[0] == "pixels" or  data[0] == "iterations":
                self.config[data[0]] = int(data[1])
            elif data[0] == "centerx" or data[0] == "centery" or data[0] == "axislength":
                self.config[data[0]] = float(data[1])
            else:
                self.config[data[0]] = data[1]


        file.close()








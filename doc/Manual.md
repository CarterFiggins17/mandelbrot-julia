Carter Figgins
Mandelbrot and Julia Manual
The program displays a Mandelbrot or Julia picture. Using the data file there is information called Fractals. The fractals have what type of picture they are (Mandelbrot or Julia) and also an axisLength, pixels, centerX, century, and Iterations. The Type Julia have two more things called cReal and cImag.
axisLength is a positive double. This I how zoomed in your picture will be. The lower the number the closer you will be. The max is 4.
pixels is an positive int. gives the size of the picture
centerX is where you want to look at on the x axis
century is where you want to look at on the y axis
Iterations are how many different colors you want between two colors. 
For type Julia
cReal is a real number used for Julia
cImag is a complex number used for Julia

Example of a Fractal for Mandelbrot
 type: Mandelbrot
pixels: 640
centerX: 0.0
centerY: 0.0
axisLength: 4.0
iterations: 100

Example of a Fractal for Julia
type: Julia
cReal: -1
cImag: 0
pixels: 1024
centerX: 0.0
centerY: 0.0
axisLength: 4.0
iterations: 78

Example of a Fractal for Mandelbrot Squared by 5
type: MandelbrotSqur5
pixels: 640
centerX: 0.0
centerY: 0.0
axisLength: 4.0
iterations: 100

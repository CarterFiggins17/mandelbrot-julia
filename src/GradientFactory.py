# creats a gradient specified by the user

import sys
import TurquoiseToRed, BlackAndWhite, MultiColor

def makeGradient(iterations):
    gradient = MultiColor.MultiColor(iterations)
    if(len(sys.argv) < 3):
        # Default
        pass
    else:
        grad = sys.argv[2]
        if grad == "TurquoiseToRed":
            gradient = TurquoiseToRed.TurquoiseToRed(iterations)
        elif grad == "BlackAndWhite":
            gradient = BlackAndWhite.BlackAndWhite(iterations)
            pass


    return gradient
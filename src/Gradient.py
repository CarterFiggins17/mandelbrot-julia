import Color


class Gradient():
    def __init__(self, iterations):
        self.iterations = iterations
        self.colors = []
        self.start = Color.Color(255, 255, 255)
        self.stop = Color.Color(0, 255, 255)
        self.dRed = 0
        self.dGrn = 0
        self.dBlu = 0


    def makeDiff(self, steps, start, stop):
        self.dRed = (stop.r - start.r) / (steps - 1)
        self.dGrn = (stop.g - start.g) / (steps - 1)
        self.dBlu = (stop.b - start.b) / (steps - 1)


    def makeGradient(self):
        self.makeDiff(self.iterations, self.start, self.stop)
        hRed = self.start.r
        hGrn = self.start.g
        hBlu = self.start.b
        for i in range(0, self.iterations):
            rgb = (int(self.start.r), int(self.start.g), int(self.start.b))
            theColor = f'#{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}'
            self.colors.append(theColor)
            self.start.r = self.start.r + self.dRed
            self.start.g = self.start.g + self.dGrn
            self.start.b = self.start.b + self.dBlu
        self.start.r = hRed
        self.start.g = hGrn
        self.start.b = hBlu

    def makeGradient2(self, iterations, start, stop):
        self.makeDiff(iterations, start, stop)
        hRed = start.r
        hGrn = start.g
        hBlu = start.b
        for i in range(0, iterations):
            rgb = (int(start.r), int(start.g), int(start.b))
            theColor = f'#{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}'
            self.colors.append(theColor)
            start.r = start.r + self.dRed
            start.g = start.g + self.dGrn
            start.b = start.b + self.dBlu
        start.r = hRed
        start.g = hGrn
        start.b = hBlu

    def setStart (self, start):
        self.start = start

    def setStop(self, stop):
        self.stop = stop

    def getColor(self, n):
        return self.colors[n-1]

    def getFirstColor(self):
        return self.colors[0]

    def getLastColor(self):
        return self.colors[-1]

    def getNumColors(self):
        return len(self.colors)


from Gradient import Gradient
import Color


class MultiColor(Gradient):
    # TODO: RETHINK THIS. im not sure how to inherit

    def __init__(self, iterations):
        super().__init__(iterations)
        self.iterations = iterations

    def makeGradient(self):
        div = int(self.iterations / 3)
        red = Color.Color(255, 0, 0)
        blue = Color.Color(0, 0, 255)
        green = Color.Color(0, 255, 0)
        yellow = Color.Color(255, 255, 0)
        pink = Color.Color(255, 0, 255)
        orange = Color.Color(255, 150, 0)

        super().makeGradient2(div, red, orange)
        super().makeGradient2(div, yellow, green)
        super().makeGradient2((div + 1), pink, blue)
